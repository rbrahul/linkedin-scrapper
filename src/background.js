//@flow
import * as constants from './constants';
import * as DB from './storage-helper';
import { openNewTab } from './utils';

type CompanyType = {
  url: string,
  scrapped: boolean
};
const updateCompanyStatusToScrapped = (companyURL: string) => {
    const companies = DB.get(constants.dbNameCompanyURL);
    companies.map((company: CompanyType): CompanyType => {
        const companyObj = company;
        if (company.url === companyURL) {
            companyObj.scrapped = true;
        }
        return companyObj;
    });
    DB.set(constants.dbNameCompanyURL, companies);
};
const searchCompanyURL = () => {
    if (DB.get(constants.companySearchingStatusDBName) === constants.companySearchingSettings.readyToGo) {
        DB.set(constants.companySearchingStatusDBName, constants.companySearchingSettings.scrapping);
    }
    if (DB.get(constants.companySearchingStatusDBName) !== constants.companySearchingSettings.scrapping) {
        return;
    }
    const allURLs = DB.get(constants.dbNameCompanyURL);
    if (allURLs) {
        const remainingURLs = allURLs.filter((companyURL: Object): any => {
            return companyURL.scrapped === false;
        });
        if (remainingURLs.length) {
            const linkedInURL = `https://www.linkedin.com/search/results/people/?keywords=${remainingURLs[0].url}&origin=SWITCH_SEARCH_VERTICAL&SEARCHING_COMPANY_URL=true`;
            openNewTab(linkedInURL, true);
        } else {
            DB.set(constants.companySearchingStatusDBName, constants.companySearchingSettings.readyToDownload);
            const linkedInURL = `https://www.linkedin.com/search/results/people/?keywords=${allURLs[0].url}&DOWNLOAD_COMPANY_SEARCHED_PROFILES=true`;
            const options = {
                url: linkedInURL,
                active: true,
            };
            chrome.tabs.create(options);
        }
    } else {
        throw new Error('No company URL available');
    }
};

const searchCompanyURLManually = () => {
    DB.set(constants.companySearchingStatusDBName, constants.companySearchingSettings.scrapping);
    DB.set(constants.dbNameCurrentCompanyURL, 0);
    const allURLs = DB.get(constants.dbNameCompanyURL);
    if (allURLs) {
        const linkedInURL = `https://www.linkedin.com/search/results/people/?keywords=${allURLs[0].url}&origin=SWITCH_SEARCH_VERTICAL&MANUAL_COMPANY_SEARCHING=true`;
        openNewTab(linkedInURL, true);
    } else {
        throw new Error('No company URL available');
    }
};

chrome.runtime.onMessage.addListener((message: any) => {
    switch (message.action) {
    case 'open_new_tab': {
        const options = {
            url: message.url,
            active: false,
        };
        chrome.tabs.create(options);
        break;
    }
    case 'scrapped_company_info': {
        chrome.tabs.query({}, (tabs) => {
            tabs.forEach((tab) => {
                const data = {
                    action: 'scrapped_company',
                };
                chrome.tabs.sendMessage(tab.id, data);
            });
        });
        break;
    }
    case 'DOWNLOADED_COMPANY_SEARCHED_PROFILES': {
        DB.set(constants.companySearchingStatusDBName, constants.companySearchingSettings.readyToGo);
        DB.set(constants.dbNameCurrentCompanyURL, 0);
        break;
    }
    case 'start_scrapping_company_urls': {
        searchCompanyURL();
        break;
    }
    case 'start_scrapping_company_urls_manually': {
        searchCompanyURLManually();
        break;
    }
    case 'company_searched_profile_saved': {
        updateCompanyStatusToScrapped(message.payload.company);
        searchCompanyURL();
        break;
    }
    case 'go_to_next_company_url': {
        chrome.tabs.query({ active: true, currentWindow: true }, (tabs: Array<any>) => {
            chrome.tabs.remove(tabs.map((tab: any): void => tab.id));
        });
        const currentCompanyURLIndex: number = DB.get(constants.dbNameCurrentCompanyURL) || 0;
        const allURLs = DB.get(constants.dbNameCompanyURL);
        if (allURLs[currentCompanyURLIndex + 1]) {
            const nextCompanyURL = allURLs[currentCompanyURLIndex + 1];
            DB.set(constants.dbNameCurrentCompanyURL, currentCompanyURLIndex + 1);
            const linkedInURL = `https://www.linkedin.com/search/results/people/?keywords=${nextCompanyURL.url}&origin=SWITCH_SEARCH_VERTICAL&MANUAL_COMPANY_SEARCHING=true`;
            openNewTab(linkedInURL, true);
        } else {
            DB.set(constants.companySearchingStatusDBName, constants.companySearchingSettings.readyToDownload);
            const linkedInURL = `https://www.linkedin.com/search/results/people/?keywords=${allURLs[0].url}&DOWNLOAD_COMPANY_SEARCHED_PROFILES=true`;
            const options = {
                url: linkedInURL,
                active: true,
            };
            chrome.tabs.create(options);
        }
        break;
    }
    default:
        break;
    }
});
