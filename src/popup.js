//@flow
import $ from 'jquery';
import { sendMessageToAllTabs, parseFileAsURLCollection, sendMessage, openNewTab } from './utils';
import * as constants from './constants';
import * as DB from './storage-helper';
import { showCompanyURLSearcingStopBtn } from './actions';

class Popup {
    companies = [];
    constructor() {
        this.initEvents();
    }
    static enableStartPan(companies: Array<any>) {
        $('#file-section').addClass('hide');
        $('#company-details').removeClass('hide');
        $('#start-btn').removeClass('hide');
        $('#start-manually-btn').removeClass('hide');
        $('#upload-btn').addClass('hide');
        $('#total-companies').text(companies.length);
    }
    processFile() {
        const fileContent = event.target.result;
        this.companies = parseFileAsURLCollection(fileContent);
        DB.set(constants.dbNameCompanyURL, this.companies);
        sendMessageToAllTabs('save_company_urls', { companies: this.companies });
        Popup.enableStartPan(this.companies);
    }
    handleFileUpload() {
        $('#upload-btn').click((e: any) => {
            e.preventDefault();
            const readers = new FileReader();
            readers.onload = this.processFile;
            if (typeof document.getElementById('csv-file-field').files[0] === 'undefined') {
                alert('File field is empty');
                return;
            }
            readers.readAsText(document.getElementById('csv-file-field').files[0]);
        });
    }
    static handleStartBtnClick() {
        $('#start-btn').click((e: any) => {
            e.preventDefault();
            sendMessage({ action: 'start_scrapping_company_urls', companies: Popup.companies });
            showCompanyURLSearcingStopBtn();
        });
    }
    static handleManualStartBtnClick() {
        $('#start-manually-btn').click((e: any) => {
            e.preventDefault();
            sendMessage({ action: 'start_scrapping_company_urls_manually' });
            showCompanyURLSearcingStopBtn();
        });
    }
    static handleStopBtnClick() {
        $('#stop-btn').click((e: any) => {
            e.preventDefault();
            const linkedInURL = 'https://www.linkedin.com/search/results/people/?keywords=aaroza.com&DOWNLOAD_COMPANY_SEARCHED_PROFILES=true';
            openNewTab(linkedInURL, true);
            DB.set(constants.companySearchingStatusDBName, constants.companySearchingSettings.readyToGo);
            window.close();
        });
    }
    static initCompanySearchingStatus() {
        if (!DB.get(constants.companySearchingStatusDBName)) {
            DB.set(constants.companySearchingStatusDBName, constants.companySearchingSettings.readyToGo);
        }
    }
    static displaySegmentBasedOnProgress() {
        if (DB.get(constants.companySearchingStatusDBName) && DB.get(constants.companySearchingStatusDBName) === constants.companySearchingSettings.scrapping) {
            showCompanyURLSearcingStopBtn();
        }
    }
    initEvents() {
        $(document).ready(() => {
            Popup.initCompanySearchingStatus();
            Popup.displaySegmentBasedOnProgress();
            this.handleFileUpload();
            Popup.handleStartBtnClick();
            Popup.handleManualStartBtnClick();
            Popup.handleStopBtnClick();
        });
    }
}

new Popup();
