// @flow

import $ from 'jquery';
import * as constants from './constants';

export const parseClassName = (className: string): string => {
    return className.replace(/\./g, '');
};
export const parseId = (id: string): string => {
    return id.replace(/#/g, '');
};
export const createScrapButton = () => {
    const button:string = `<div class="scrapp-button-holder">
    <div class=""></div>
    <a href="#" class="${parseClassName(constants.scrapperButton)}" style="color:#0084BE">Start</a>
    <a href="#" class="${parseClassName(constants.cancelScrappingButton)}" style="color:#0084BE;display:none;">Stop</a>
    </div>`;
    $(constants.buttonPlaceHolder).append(button);
};

export const createScrappingContentDownloadModal = () => {
    const documentHeight = $(document).height();
    const loader:string = `<div class="ce-scrapper-modal" id="${parseId(constants.downloadCSVModal)}" style='height:${documentHeight}px'>
            <div class="ce-scrapper-modal-content">
                <div class="ce-scrapped-popup-description-area">
                    <h3> Total <span id="${parseId(constants.scrappedInfoSelector.totalProfilesCounterSelector)}">0</span>
                    Profiles and <span id="${parseId(constants.scrappedInfoSelector.totalCompaniesCounterSelector)}">0</span>
                    Company's information has been saved</h3>
                </div>
                <div class="ce-scrapped-button-area">
                <button type="button" class="${parseClassName(constants.downloadAsCSVBtn)}">Download as CSV</button>
                <button type="button" class="${parseClassName(constants.cancelDownloadBtn)}">Cancel Download</button>
                </div>
            </div> 
    </div>`;
    $('body').append(loader);
};

export const showCompanySearchDownloadModal = () => {
    const documentHeight = $(document).height();
    const loader:string = `<div class="ce-scrapper-modal" id="${parseId(constants.companySearchProfileDowloadModal.modalId)}" style='height:${documentHeight}px'>
            <div class="ce-scrapper-modal-content">
                <div class="ce-scrapped-popup-description-area">
                    <h3> Total <span id="${parseId(constants.scrappedInfoSelector.totalProfilesCounterSelector)}">0</span>
                    Profiles have been saved</h3>
                </div>
                <div class="ce-scrapped-button-area">
                <button type="button" class="${parseClassName(constants.companySearchProfileDowloadModal.downloadAsCSVBtn)}">Download as CSV</button>
                <button type="button" class="${parseClassName(constants.companySearchProfileDowloadModal.cancelDownload)}">Cancel Download</button>
                </div>
            </div> 
    </div>`;
    $('body').append(loader);
};

export const showSelectableScrappedProfilesModal = () => {
    const documentHeight = $(document).height();
    const loader:string = `<div class="ce-scrapper-modal" id="${parseId(constants.seletableProfilesModal)}" style='height:${documentHeight}px; display:none;'>
            <div class="ce-scrapper-modal-content ce-popup-large" style="padding: 20px;">
            <h3 class="ce-title">Select Profiles</h3>
            <div class="profiles-grid">
                <table class="table table-bordered ce-table">
                    <thead>
                    <tr>
                        <th><input type="checkbox" class="profile-checkbox-all" value="1"/>All</th>
                        <th>Full Name</th>
                        <th>Designation</th>
                        <th>Company</th>
                        <th>Location</th>
                        <th>Website</th>
                    </tr>
                    </thead>
                    <tbody id="${parseId(constants.scrappedSelectableProfiles)}">
                    <tr>
                        <td><input type="checkbox" class="profile-checkbox" value="1"/></td>
                        <td>Rahul Baruri</td>
                        <td>Fullstack Developer</td>
                        <td>Aaroza</td>
                        <td>Dhaka Bangladesh</td>
                        <td>aarozainfotech.com</td>
                    </tr>
                    </tbody>
                </table>
            </div>
                <div class="ce-scrapped-button-area" style="padding:30px;">
                <a href="#" class="${parseClassName(constants.saveSelectedProfilesBtn)}">Save</a>
                <a href="#" class="ce-btn ${parseClassName(constants.cancelSelectableModalBtn)}">Cancel</a>
                </div>
            </div> 
    </div>`;
    $('body').append(loader);
};

export const createManualScrappingButton = () => {
    const button:string = `<div class="${parseClassName(constants.manualScrappingButtonGroup)}">
    <a href="#" class="ce-btn ${parseClassName(constants.manualScrapButtonBtn)}" style="color:#0084BE">Grab this Page</a>
    <a href="#" class="ce-btn ${parseClassName(constants.manualVisitPageBtn)}" style="color:#0084BE">Visit Website</a>
    <a href="#" class="ce-btn ${parseClassName(constants.manualNextCompanyButtonBtn)}" style="color:#0084BE">Next</a>
    </div>`;
    $(constants.manualButtonPlaceHolder).after(button);
};
export const createManualGrabbedSuccessAlert = () => {
    $('.sub-nav').after('<div class="ce-alert-success center-align">Data grabbed Successfully</div>');
};
