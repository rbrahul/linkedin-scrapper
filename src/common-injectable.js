//@flow
import $ from 'jquery';
import * as constants from './constants';
import * as DB from './storage-helper';
import {
    sendMessage,
    queryParams,
    getRandomNumber,
    downloadSearchedCompanyProfiles,
    resetCompanySearchedProfileDB,
} from './utils';

import {
    showCompanySearchDownloadModal,
    createManualScrappingButton,
    createManualGrabbedSuccessAlert,
    showSelectableScrappedProfilesModal,
} from './components';

window.manuallyGrabbedProfiles = [];

chrome.runtime.onMessage.addListener((message: any) => {
    console.info(message);
    switch (message.action) {
    case 'save_company_urls':
        DB.set(constants.dbNameCompanyURL, message.payload.companies);
        break;
    case 'start_scrapping_company_urls':
        break;
    default:
        break;
    }
});
const showCompanySearchedResultDownloadModal = () => {
    $(document).ready(() => {
        if (window.location.href.includes('DOWNLOAD_COMPANY_SEARCHED_PROFILES')) {
            const profiles: Array<any> = DB.get(constants.dbNameCompanySearchedProfiles) || [];
            showCompanySearchDownloadModal();
            $(constants.scrappedInfoSelector.totalProfilesCounterSelector).text(profiles.length);
        }
    });
};
const isCompanySearchResultPage = (): boolean => {
    return window.location.href.includes('SEARCHING_COMPANY_URL');
};

const isManualCompanySearchPage = (): boolean => {
    return window.location.href.includes('MANUAL_COMPANY_SEARCHING');
};
const grabPage = () => {
    if ($(constants.companySearchResultItem).length) {
        const profiles: Array<any> = [];
        const companyWebsite: string = queryParams().keywords;
        $.each($(constants.companySearchResultItem), (index: number, item: Node) => {
            const profileInfo = {
                companyWebsite,
                profileName: '',
                designation: '',
                companyName: '',
                location: '',
            };
            const nameNode = $(item).find(constants.searchResultInfo.name);
            const designationNode = $(item).find(constants.searchResultInfo.designation);
            const locationNode = $(item).find(constants.searchResultInfo.location);
            if (nameNode) {
                profileInfo.profileName = nameNode.text().trim();
            }
            if (designationNode) {
                const designation = designationNode.text().trim();
                profileInfo.designation = designation;
                const positionOfAs = designation.lastIndexOf('at ');
                if (positionOfAs > -1) {
                    profileInfo.companyName = designation.substring(positionOfAs + 3);
                }
            }
            if (locationNode) {
                profileInfo.location = locationNode.text().trim();
            }
            profiles.push(profileInfo);
        });
        const existingCompanySearchedProfiels = DB.get(constants.dbNameCompanySearchedProfiles) || [];
        const withNewProfiles = [...existingCompanySearchedProfiels, ...profiles];
        DB.set(constants.dbNameCompanySearchedProfiles, withNewProfiles);
    }
};
const generateSelectableRows = () => {
    let rows: string = '';
    window.manuallyGrabbedProfiles.forEach((item: any, index: number) => {
        const node: string = `<tr>
                        <td><input type="checkbox" class="profile-checkbox" value="${index}"/></td>
                        <td>${item.profileName}</td>
                        <td>${item.designation}</td>
                        <td>${item.companyName}</td>
                        <td>${item.location}</td>
                        <td><a href="${item.companyWebsite}">${item.companyWebsite}</a></td>
                    </tr>`;
        rows += node;
    });
    $(constants.scrappedSelectableProfiles).html(rows);
};

const grabCurrentPageManually = () => {
    if ($(constants.companySearchResultItem).length) {
        const profiles: Array<any> = [];
        const companyWebsite: string = queryParams().keywords;
        $.each($(constants.companySearchResultItem), (index: number, item: Node) => {
            const profileInfo = {
                companyWebsite,
                profileName: '',
                designation: '',
                companyName: '',
                location: '',
            };
            const nameNode = $(item).find(constants.searchResultInfo.name);
            const designationNode = $(item).find(constants.searchResultInfo.designation);
            const locationNode = $(item).find(constants.searchResultInfo.location);
            if (nameNode) {
                profileInfo.profileName = nameNode.text().trim();
            }
            if (designationNode) {
                const designation = designationNode.text().trim();
                profileInfo.designation = designation;
                const positionOfAs = designation.lastIndexOf('at ');
                if (positionOfAs > -1) {
                    profileInfo.companyName = designation.substring(positionOfAs + 3);
                }
            }
            if (locationNode) {
                profileInfo.location = locationNode.text().trim();
            }
            profiles.push(profileInfo);
        });
        window.manuallyGrabbedProfiles = profiles;
        generateSelectableRows();
        window.scroll(0, 0);
        $(constants.seletableProfilesModal).show();
    }
};

const startScrappingForCompanyURLs = () => {
    $(document).ready(() => {
        if (isCompanySearchResultPage() && !isManualCompanySearchPage()) {
            const lastRandomNumber: number = DB.get(constants.lastRandomNumberDBName) || 15;
            const randomNumber: number = getRandomNumber(constants.timeInterValForAsyncCompanySearching.minimum, constants.timeInterValForAsyncCompanySearching.maximum, lastRandomNumber);
            DB.set(constants.lastRandomNumberDBName, randomNumber);
            setTimeout(() => {
                //window.scrollTo(0, document.body.scrollHeight || document.documentElement.scrollHeight);
                window.scroll(0, 1000);
            }, 8 * 1000);
            setTimeout(() => {
                grabPage();
                sendMessage({
                    action: 'company_searched_profile_saved',
                    payload: { company: queryParams().keywords }
                });
                window.close();
            }, randomNumber * 1000);
        }
    });
};

const closeSearchedProfileModal = () => {
    $(constants.companySearchProfileDowloadModal.modalId).hide();
    resetCompanySearchedProfileDB();
};

const scrappCompanyManually = () => {
    $(document).ready(() => {
        setTimeout(() => {
            if (isManualCompanySearchPage()) {
                createManualScrappingButton();
                if (!$(constants.companySearchResultItem).length) {
                    $(constants.manualScrapButtonBtn).hide();
                }
            }
        }, 3 * 1000);
    });
};

const cancelSelectableProfilesModal = () => {
    $(constants.seletableProfilesModal).hide();
    $(constants.allSelectingCheckbox).prop('checked', false);
};
const saveSelectedProfilesOnly = () => {
    if (!$(constants.profileSelectingCheckbox)) {
        alert('No Profile Selected');
        return;
    }
    let onlySelectedProfiles = [];
    $.each($(constants.profileSelectingCheckbox), (index: number, item: any) => {
        if ($(item).is(':checked')) {
            const selectedProfileIndex = $(item).val();
            if (selectedProfileIndex && window.manuallyGrabbedProfiles[selectedProfileIndex]) {
                onlySelectedProfiles = [...onlySelectedProfiles, ...[window.manuallyGrabbedProfiles[selectedProfileIndex]]];
            }
        }
    });
    if (onlySelectedProfiles.length) {
        const existingCompanySearchedProfiels = DB.get(constants.dbNameCompanySearchedProfiles) || [];
        const withNewProfiles = [...existingCompanySearchedProfiels, ...onlySelectedProfiles];
        DB.set(constants.dbNameCompanySearchedProfiles, withNewProfiles);
        sendMessage({
            action: 'manual_company_searched_profile_scrapped',
            payload: { company: queryParams().keywords }
        });
        cancelSelectableProfilesModal();
        if ($('.ce-alert-success').length) {
            $('.ce-alert-success').show();
        } else {
            createManualGrabbedSuccessAlert();
        }
        setTimeout(() => {
            $('.ce-alert-success').fadeOut();
        }, 5 * 1000);
    }
};

const scrapCurrentPage = (e) => {
    e.preventDefault();
  //  $(constants.manualScrapButtonBtn).hide();
    window.scroll(0, 1000);
    setTimeout(() => {
        grabCurrentPageManually();
    }, 5 * 1000);
};

const gotoNextCompanyURL = (e) => {
    e.preventDefault();
    sendMessage({
        action: 'go_to_next_company_url',
    });
};

(() => {
    $(() => {
        $(document).on('click', constants.companySearchProfileDowloadModal.cancelDownload, () => {
            closeSearchedProfileModal();
        });
        $(document).on('click', constants.companySearchProfileDowloadModal.downloadAsCSVBtn, () => {
            downloadSearchedCompanyProfiles();
            closeSearchedProfileModal();
            resetCompanySearchedProfileDB();
            sendMessage({ action: 'DOWNLOADED_COMPANY_SEARCHED_PROFILES' });
        });
        $(document).on('click', constants.manualScrapButtonBtn, scrapCurrentPage);
        $(document).on('click', constants.manualNextCompanyButtonBtn, gotoNextCompanyURL);
        $(document).on('click', constants.manualVisitPageBtn, (e) => {
            e.preventDefault();
            const win = window.open(`http://${queryParams().keywords}`, '_blank');
            win.focus();
        });
        $(document).on('click', constants.cancelSelectableModalBtn, (e) => {
            e.preventDefault();
            $(constants.seletableProfilesModal).hide();
        });
        $(document).on('change', constants.allSelectingCheckbox, (e) => {
            e.preventDefault();
            if ($(e.target).is(':checked')) {
                $('.profile-checkbox').prop('checked', true);
            } else {
                $('.profile-checkbox').prop('checked', false);
            }
        });
        $(document).on('click', constants.saveSelectedProfilesBtn, () => {
            saveSelectedProfilesOnly();
        });
    });
})();


scrappCompanyManually();
startScrappingForCompanyURLs();
showCompanySearchedResultDownloadModal();
showSelectableScrappedProfilesModal();
