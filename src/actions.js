// @flow
import $ from 'jquery';
import * as constants from './constants';
import * as DB from './storage-helper';
import {
    openCompanyURLInNewTab,
    resetScrapper,
    fetchAllCompanyURLs,
    download,
    openDownloadModal,
    downloadSearchiedCompanyProfiles,
} from './utils';

export const startScrapping = (e) => {
    e.preventDefault();
    try {
        $(constants.scrapperButton).hide();
        $(constants.cancelScrappingButton).show();
        window.companyProfileURLs = fetchAllCompanyURLs();
        openCompanyURLInNewTab();
        window.runningScrapping = true;
    } catch (e) {
        console.error('LINKED SCRAPPER ERROR', e);
    }
};

export const cancelScrapping = (e) => {
    e.preventDefault();
    if (DB.get(constants.dbName) && DB.get(constants.dbName).length) {
        window.cancelScrapping = true;
        openDownloadModal();
    } else {
        resetScrapper();
    }
};

export const closeCSVDownloadModal = () => {
    $(constants.downloadCSVModal).fadeOut();
    resetScrapper();
};

export const downloadAsCSV = () => {
    download();
    $(constants.downloadCSVModal).slideUp();
    resetScrapper();
};


export const cancelDownload = () => {
    const cancelDownloadConfirm = confirm('If you cancel download then the scrapped data will be lost due to resetting.\n Do you really want to continue?');
    if (cancelDownloadConfirm === true) {
        closeCSVDownloadModal();
        resetScrapper();
    }
};

export const closingPageHandler = (): any => {
    if (window.runningScrapping) return 'Scrapping is running on that page. Scrapping will be discarded if you reload or change the webpage.\n Do you really want to continue?';
};

export const showCompanyURLSearcingStopBtn = () => {
    $('#file-section').addClass('hide');
    $('#company-details').addClass('hide');
    $('#stop-progress-segment').addClass('hide');
    $('#start-btn').addClass('hide');
    $('#start-manually-btn').addClass('hide');
    $('#upload-btn').addClass('hide');
    $('#stop-btn').removeClass('hide');
    $('#stop-progress-segment').removeClass('hide');
};

export const downloadSearchedCompayProfilesAsCSV = () => {
    downloadSearchiedCompanyProfiles();
};
