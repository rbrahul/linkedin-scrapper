// @flow
export const buttonPlaceHolder:string = '.spotlights-wrapper';
export const companyItemSelector:string = 'ul#results-list > li';
export const scrapperButton:string = '.scrap-companies-btn';
export const cancelScrappingButton:string = '.cancel-scrapping-btn';
export const pageNumber:string = '.area';
export const selectedPage:string = '.current-page';
export const companyResutlItemSelector = 'li.result.loading.company';
export const companyNameURLSelector:string = 'h4.name > a.name-link.account-link';
export const nextPageSelector:string = 'a.next-pagination.page-link';
export const leadRecommendationList:string = 'ul.lead-recommendations-list li.entity-card.recommended';
export const leadRecommendationUserInfo = {
    profileName: '.profile-info > h3.profile-name > a',
    designation: 'p.title',
    location: 'p.location',
    website: 'li.account-term > h3.account-term-title', //contains Website or not node
    companyName: 'h1.acct-name',
};
export const companySearchResultPageURL:string = 'https://www.linkedin.com/sales/search/companies';
export const baseURL:string = 'https://www.linkedin.com';

export const dbName = 'mn-company-profiles';
export const dbNameCompanyURL = 'ce-company-urls';
export const dbNameCompanySearchedProfiles: string = 'ce-company-searched-profiles';
export const dbNameCurrentCompanyURL: string = 'ce-current-company-url';
export const lastIntervalDBName: string = 'ce-last-interval';
export const lastRandomNumberDBName: string = 'ce-last-random-number';
export const fileName = 'LinkedIn-Scrapped-Profiles';
export const fileNameOfCompanyProfiles = 'LinkedIn-Scrapped-Company-Searched-Profiles';

export const downloadCSVModal:string = '#ce-scrapped-data-downloading-modal';
export const downloadAsCSVBtn:string = '.ce-csv-download-btn';
export const cancelDownloadBtn:string = '.ce-csv-download-cancel-btn';
export const scrappedInfoSelector = {
    totalCompaniesCounterSelector: '#total-company-counter',
    totalProfilesCounterSelector: '#total-profiles-counter'
};
export const timeInterValForAsyncScrapping = {
    minimum: 5,
    maximum: 10,
};

// Company Profile Search Result configarations
export const companySearchResultItem: string = '.search-result.search-result__occluded-item';
export const searchResultInfo: any = {
    name: 'span.name.actor-name',
    designation: '.search-result__info > p.subline-level-1',
    location: '.search-result__info > p.subline-level-2',
};
export const timeInterValForAsyncCompanySearching = {
    minimum: 15,
    maximum: 30,
};
export const companySearchingStatusDBName: string = 'ce-linked-company-searching-status';
export const companySearchingSettings = {
    readyToGo: 0,
    scrapping: 1,
    readyToDownload: 2,
    paused: 3,
};
export const companySearchProfileDowloadModal = {
    modalId: '#searched-company-profile-modal',
    downloadAsCSVBtn: '.download-searched-profile',
    cancelDownload: '.cancel-searched-profile-download'
};
export const manualButtonPlaceHolder: string = '.type-ahead-input-container';
export const manualScrappingButtonGroup: string = '.ce-scrapp-button-group';
export const manualScrapButtonBtn: string = '.scrap-manually';
export const manualNextCompanyButtonBtn: string = '.next-company-btn';
export const manualVisitPageBtn: string = '.manual-visit-page-btn';

export const cancelSelectableModalBtn: string = '.cancel-selection-saving';
export const saveSelectedProfilesBtn: string = '.save-selected-profiles-btn';
export const seletableProfilesModal: string = '#selectableProfilesModal';
export const allSelectingCheckbox: string = '.profile-checkbox-all';
export const profileSelectingCheckbox: string = '.profile-checkbox';
export const scrappedSelectableProfiles: string = '#scrapped-selectable-profiles';
