import $ from 'jquery';
import { sendMessage, getRandomNumber } from './utils';
import * as constants from './constants';
import * as DB from './storage-helper';

$(document).ready(() => {
    let i = 0;
    const lastInterVal = DB.get(constants.lastIntervalDBName) || 5;
    const interval = setInterval(() => {
        const profiles = [];
        if (i > 15) {
            clearInterval(interval);
            if (document.location.href.includes('VISITED_VIA_EXTENSION')) {
                sendMessage({
                    action: 'scrapped_company_info',
                    payLoad: profiles,
                });
                window.close();
            }
        }
        if ($(constants.leadRecommendationList).length) {
            clearInterval(interval);
            const existingProfiles = DB.get(constants.dbName) || [];
            $.each($(constants.leadRecommendationList), (index, card) => {
                const profileInfo = {
                    companyWebsite: '',
                    profileName: '',
                    designation: '',
                    companyName: '',
                    location: '',
                };
                const profileNameNode = $(card).find(constants.leadRecommendationUserInfo.profileName);
                const locationNode = $(card).find(constants.leadRecommendationUserInfo.location);
                const designationNode = $(card).find(constants.leadRecommendationUserInfo.designation);
                const companyNameNode = $(constants.leadRecommendationUserInfo.companyName);
                const companyWebsiteNode = $(constants.leadRecommendationUserInfo.website).last();
                if (profileNameNode) {
                    profileInfo.profileName = profileNameNode.text().trim();
                }
                if (companyNameNode) {
                    profileInfo.companyName = companyNameNode.text().trim();
                }
                if (locationNode) {
                    profileInfo.location = locationNode.text().trim();
                }
                if (designationNode) {
                    profileInfo.designation = designationNode.text().trim();
                }
                if (companyWebsiteNode) {
                    const websiteNode = $(companyWebsiteNode).next('p.account-term-desc').find('a');
                    if (websiteNode.length) {
                        profileInfo.companyWebsite = websiteNode.attr('href');
                    }
                }
                profiles.push(profileInfo);
            });
            const newProfiles = existingProfiles.concat(profiles);
            DB.set(constants.dbName, newProfiles);
            const currentInterval = getRandomNumber(constants.timeInterValForAsyncScrapping.minimum, constants.timeInterValForAsyncScrapping.maximum, lastInterVal);
            setTimeout((currentAsyncDuration) => {
                if (document.location.href.includes('VISITED_VIA_EXTENSION')) {
                    DB.set(constants.lastIntervalDBName, currentAsyncDuration);
                    sendMessage({
                        action: 'scrapped_company_info',
                    });
                    window.close();
                }
            }, currentInterval * 1000, currentInterval);
        }
        i += 1;
    }, 1000);
});

