// @flow
/*eslint class-methods-use-this: "error"*/
/*eslint-env es6*/
import $ from 'jquery';
import * as components from './components';
import * as constants from './constants';
import attachEvents from './bind-events';
import { receiveMessage, resetStorage } from './utils';

window.companyProfileURLs = [];
window.currentCompanyIndex = 0;
window.cancelScrapping = false;
window.runningScrapping = false;
window.totalCompayScrapped = 0;

class App {
    static initiateScrapperOnCompanyPage() {
        if (document.location.href.includes(constants.companySearchResultPageURL)) {
            components.createScrapButton();
            components.createScrappingContentDownloadModal();
        }
    }
    static initEvents() {
        $(document).ready(() => {
            resetStorage();
            receiveMessage();
            App.initiateScrapperOnCompanyPage();
            attachEvents();
        });
    }
}

App.initEvents();
