// @flow
export const toJSON = (data: any): string => {
    return JSON.stringify(data);
};

export const parseJSON = (data: any): any => {
    return JSON.parse(data);
};
export const isLocalStorageEnabled = (): any => {
    if (!localStorage) {
        throw new Error('Your browser doesn\'t supports localStorage or may be deactivated from developer console');
    }
};

export const set = (key: string, value: any): any => {
    isLocalStorageEnabled();
    const data = toJSON(value);
    localStorage.setItem(key, data);
};

export const get = (key: string): any => {
    isLocalStorageEnabled();
    return (typeof localStorage[key] === 'undefined') ? null : parseJSON(localStorage[key]);
};


/**
 * Get data from nested level
 * @param {users->rahul->subjects->0}key 
 */
export const getNested = (key: string): any => {
    isLocalStorageEnabled();
    let storage = localStorage;
    if (key.includes('->')) {
        const keys = key.split('->');
        keys.forEach((keyItem: string) => {
            const index = !isNaN(keyItem) ? Number(keyItem) : keyItem;
            if (typeof storage[index] === 'undefined') {
                throw new Error('No property found in localStorage');
            }
            try {
                storage = JSON.parse(storage[index]);
            } catch (e) {
                storage = storage[index];
            }
        });
        return storage;
    }
    return localStorage[key] || null;
};
