import $ from 'jquery';
import * as actions from './actions';
import * as constants from './constants';

export default function () {
    $(document).on('click', constants.scrapperButton, actions.startScrapping);
    $(document).on('click', constants.cancelScrappingButton, actions.cancelScrapping);
    $(document).on('click', constants.downloadAsCSVBtn, actions.downloadAsCSV);
    $(document).on('click', constants.cancelDownloadBtn, actions.cancelDownload);
    window.onbeforeunload = actions.closingPageHandler;
}
