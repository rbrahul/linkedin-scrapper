// @flow
import $ from 'jquery';
import * as constants from './constants';
import * as DB from './storage-helper';
import exportAsCSV from './export-as-csv';

export const fetchAllCompanyURLs = (): Array<string> => {
    const urls: Array<string> = [];
    $.each($(constants.companyResutlItemSelector), (index: number, item: Node) => {
        const url = $(item).find(constants.companyNameURLSelector).attr('href');
        const urlItem: { url: string, scrapped: boolean } = {
            url,
            scrapped: false,
        };
        urls.push(urlItem);
    });
    return urls;
};

export const sendMessage = (data) => {
    chrome.runtime.sendMessage(data);
};

export const download = () => {
    const profilesData = DB.get(constants.dbName);
    exportAsCSV(profilesData, constants.fileName, false);
};
export const downloadSearchedCompanyProfiles = () => {
    const profilesData = DB.get(constants.dbNameCompanySearchedProfiles);
    exportAsCSV(profilesData, constants.fileNameOfCompanyProfiles, false);
};

export const openCompanyURLInNewTab = () => {
    setTimeout(() => {
        if (window.companyProfileURLs.length) {
            sendMessage({ action: 'open_new_tab', url: `${constants.baseURL}${window.companyProfileURLs[window.currentCompanyIndex].url}&VISITED_VIA_EXTENSION=true` });
        } else {
            throw new Error('No Company Profile URL found');
        }
    }, Math.ceil(Math.random() * 11) * 500);
};

const setScrappedStatusToCompanyItem = () => {
    if (typeof window.companyProfileURLs[window.currentCompanyIndex].scrapped !== 'undefined') {
        window.companyProfileURLs[window.currentCompanyIndex].scrapped = true;
    }
    $(constants.companyItemSelector).eq(window.currentCompanyIndex).addClass('scrapped-company');
};

const paginateNext = () => {
    window.currentCompanyIndex = 0;
    window.companyProfileURLs = [];
    $(constants.nextPageSelector)[0].click();
    setTimeout(() => {
        window.companyProfileURLs = fetchAllCompanyURLs();
        openCompanyURLInNewTab();
    }, 7 * 1000);
};

export const openDownloadModal = () => {
    window.scrollTo(0, 0);
    const totalCompaniesScrapped: number = window.totalCompayScrapped;
    const totalProfilesScrapped: number = DB.get(constants.dbName).length;
    $(constants.scrappedInfoSelector.totalCompaniesCounterSelector).text(totalCompaniesScrapped);
    $(constants.scrappedInfoSelector.totalProfilesCounterSelector).text(totalProfilesScrapped);
    $(constants.downloadCSVModal).fadeIn();
};

export const receiveMessage = () => {
    chrome.runtime.onMessage.addListener((message: any) => {
        switch (message.action) {
        case 'scrapped_company':
            window.totalCompayScrapped += 1;
            if (window.cancelScrapping) {
                return;
            }
            setScrappedStatusToCompanyItem();
            window.currentCompanyIndex += 1;
            if (typeof window.companyProfileURLs[window.currentCompanyIndex] === 'undefined') {
                if ($(constants.nextPageSelector).length && $(constants.nextPageSelector).hasClass('disabled') === false) {
                    paginateNext();
                } else {
                    openDownloadModal();
                }
                return;
            }

            openCompanyURLInNewTab();
            break;
        default:
            break;
        }
    });
};

export const resetScrapper = () => {
    window.cancelScrapping = false;
    window.runningScrapping = false;
    window.currentCompanyIndex = 0;
    window.totalCompayScrapped = 0;
    window.companyProfileURLs = [];
    DB.set(constants.dbName, []);
    $(constants.cancelScrappingButton).hide();
    $(constants.scrapperButton).show();
    $('.scrapped-company').removeClass('scrapped-company');
};
export const resetSearchingCompanyScrapper = () => {
    DB.set(constants.companySearchingStatusDBName, constants.companySearchingSettings.readyToGo);
    DB.set(constants.dbNameCompanySearchedProfiles, []);
    DB.set(constants.dbNameCompanyURL, []);
    $(constants.cancelScrappingButton).hide();
    $(constants.scrapperButton).show();
    $('.scrapped-company').removeClass('scrapped-company');
};

export const resetStorage = () => {
    DB.set(constants.dbName, []);
};

export const getRandomNumber = (min: number, max: number, lastNumber: number): number => {
    const number = Math.ceil(Math.random() * max);
    if (number < min || number > max || number === lastNumber) {
        return getRandomNumber(min, max, lastNumber);
    }
    return number;
};

export const sendMessageToAllTabs = (action: string, data: any = null) => {
    chrome.tabs.query({ currentWindow: true }, (tabs: any) => {
        tabs.forEach((tab: any) => {
            if (tab.url.includes(constants.baseURL)) {
                const message = {
                    action,
                    payload: data || null
                };
                chrome.tabs.sendMessage(tab.id, message);
            }
        });
    });
};
export const parseFileAsURLCollection = (content: any): Array<any> => {
    let urls = content.split('\n');
    urls = urls.map((url: string): any => {
        const urlItem = { url: '', scrapped: false };
        const pattern = /https?:\/\/(www.)?/g;
        urlItem.url = url.replace(pattern, '');
        return urlItem;
    })
    .filter((urlItem: any): any => urlItem.url.trim() !== '');
    return urls;
};
export const queryParams = (urlStr?: string): any => {
    const url = urlStr || window.location.href;
    const hasQueryParams = url.indexOf('?') >= 0;
    const queries = {};
    if (!hasQueryParams) return queries;
    const queryParts = url.substring(url.indexOf('?') + 1);
    const queryArray = queryParts.split('&');
    queryArray.forEach((arrItem: string) => {
        const queryArray = arrItem.split('=');
        queries[queryArray[0]] = queryArray[1];
    });
    return queries;
};
export const resetCompanySearchedProfileDB = () => {
    DB.set(constants.dbNameCompanySearchedProfiles, []);
};

export const openNewTab = (url: string, active?: boolean = false) => {
    const options = {
        url,
        active
    };
    chrome.tabs.create(options);
};
