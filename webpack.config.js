const WebpackShellPlugin = require('webpack-shell-plugin');
const path = require('path');

const plugins = [];

plugins.push(new WebpackShellPlugin({
    onBuildStart: ['echo "Starting build process"'],
    onBuildEnd: ['npm run chrome-ready']
}));

module.exports = {
    context: path.resolve(__dirname, './src'), // eslint-disable-line
    entry: {
        main: './main.js',
        background: './background.js',
        'company-parser': './company-parser.js',
        popup: './popup.js',
        'common-injectable': './common-injectable.js'
    },
    output: {
        path: path.resolve(__dirname, './dist'), // eslint-disable-line
        filename: '[name].js',
    },
    plugins,
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: [/node_modules/],
                use: [{
                    loader: 'babel-loader',
                    options: { presets: ['es2015', 'stage-0'] },
                }],
            },
        ],
    },
};
